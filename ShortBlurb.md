There are three things that make the Kubuntu Flavour special.

First, is the award-winning [Plasma Desktop](https://kde.org/plasma-desktop/). While simple-to-use out-of-the-box, Plasma offers the greatest customizability among the Linux Desktop Environments, with many skins available  in the [KDE store](https://store.kde.org/browse/). from Global Themes to Icons, Cursors, and Widgets (Plasmoids).

Second, are the [KDE Applications](https://apps.kde.org/), in particular Dolphin, the file manager, Discover, the "app store", Kate, the Advanced Text Editor, Konversation, the IRC client, and Konsole, the virtual terminal.

Last, but perhaps most important, is the welcoming, friendly Kubuntu community.

